<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220418213455 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE dept (DEPTNO INT AUTO_INCREMENT NOT NULL COMMENT \'Department\'\'s identification number\', DNAME VARCHAR(20) NOT NULL COMMENT \'Name of the current department\', LOC VARCHAR(20) NOT NULL COMMENT \'Location of the current department\', PRIMARY KEY(DEPTNO)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE emp (EMPNO INT AUTO_INCREMENT NOT NULL, ENAME VARCHAR(20) NOT NULL, JOB VARCHAR(20) NOT NULL, HIREDATE DATE NOT NULL, SAL INT NOT NULL, COMM INT DEFAULT NULL, DEPTNO INT DEFAULT NULL COMMENT \'Department\'\'s identification number\', MGR INT DEFAULT NULL, INDEX fk_DEPTNO (DEPTNO), INDEX fk_MGR (MGR), PRIMARY KEY(EMPNO)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE proj (PROJID INT AUTO_INCREMENT NOT NULL, STARTDATE DATE NOT NULL, ENDDATE DATE NOT NULL, EMPNO INT DEFAULT NULL, INDEX fk_PROJ (EMPNO), PRIMARY KEY(PROJID)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE emp ADD CONSTRAINT FK_310BB40F9D3989A4 FOREIGN KEY (DEPTNO) REFERENCES dept (DEPTNO)');
        $this->addSql('ALTER TABLE emp ADD CONSTRAINT FK_310BB40FBD5E2E9B FOREIGN KEY (MGR) REFERENCES emp (EMPNO)');
        $this->addSql('ALTER TABLE proj ADD CONSTRAINT FK_520C3C90DB6AB0FE FOREIGN KEY (EMPNO) REFERENCES emp (EMPNO)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE emp DROP FOREIGN KEY FK_310BB40F9D3989A4');
        $this->addSql('ALTER TABLE emp DROP FOREIGN KEY FK_310BB40FBD5E2E9B');
        $this->addSql('ALTER TABLE proj DROP FOREIGN KEY FK_520C3C90DB6AB0FE');
        $this->addSql('DROP TABLE dept');
        $this->addSql('DROP TABLE emp');
        $this->addSql('DROP TABLE proj');
    }
}
