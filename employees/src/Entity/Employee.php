<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Employee
 *
 * @ORM\Table(name="emp", indexes={@ORM\Index(name="fk_DEPTNO", columns={"DEPTNO"}), @ORM\Index(name="fk_MGR", columns={"MGR"})})
 * @ORM\Entity(repositoryClass="App\Repository\EmployeeRepository")
 * @ORM\Entity
 */
class Employee
{
    /**
     * @var int
     *
     * @ORM\Column(name="EMPNO", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="ENAME", type="string", length=20, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="JOB", type="string", length=20, nullable=false)
     */
    private $job;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="HIREDATE", type="date", nullable=false)
     */
    private $hireDate;

    /**
     * @var int
     *
     * @ORM\Column(name="SAL", type="integer", nullable=false)
     */
    private $salary;

    /**
     * @var int|null
     *
     * @ORM\Column(name="COMM", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $comm = NULL;

    /**
     * @var \Department
     *
     * @ORM\ManyToOne(targetEntity="Department")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="DEPTNO", referencedColumnName="DEPTNO")
     * })
     */
    private $department;

    /**
     * @var \Employee
     *
     * @ORM\ManyToOne(targetEntity="Employee")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="MGR", referencedColumnName="EMPNO")
     * })
     */
    private $manager;

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getJob(): ?string
    {
        return $this->job;
    }

    public function setJob(string $job): self
    {
        $this->job = $job;

        return $this;
    }

    public function getHireDate(): ?\DateTimeInterface
    {
        return $this->hireDate;
    }

    public function setHireDate(\DateTimeInterface $hireDate): self
    {
        $this->hireDate = $hireDate;

        return $this;
    }

    public function getSalary(): ?int
    {
        return $this->salary;
    }

    public function setSalary(int $salary): self
    {
        $this->salary = $salary;

        return $this;
    }

    public function getComm(): ?int
    {
        return $this->comm;
    }

    public function setComm(?int $comm): self
    {
        $this->comm = $comm;

        return $this;
    }

    public function getDepartment(): ?Department
    {
        return $this->department;
    }

    public function setDepartment(?Department $department): self
    {
        $this->department = $department;

        return $this;
    }

    public function getManager(): ?self
    {
        return $this->manager;
    }

    public function setManager(?self $manager): self
    {
        $this->manager = $manager;

        return $this;
    }


}
