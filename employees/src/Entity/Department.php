<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Department
 *
 * @ORM\Table(name="dept")
 * @ORM\Entity(repositoryClass="App\Repository\DepartmentRepository")
 * @ORM\Entity
 */
class Department
{
    /**
     * @var int
     *
     * @ORM\Column(name="DEPTNO", type="integer", nullable=false, options={"comment"="Department's identification number"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="DNAME", type="string", length=20, nullable=false, options={"comment"="Name of the current department"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="LOC", type="string", length=20, nullable=false, options={"comment"="Location of the current department"})
     */
    private $location;

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(string $location): self
    {
        $this->location = $location;

        return $this;
    }


}
